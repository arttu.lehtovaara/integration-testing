const pad = (hex) => {
  return (hex.length === 1 ? '0' + hex : hex) // jos eka toteutuu lisätään nolla ja palautetaan 01, jos ei palautetaan hex
}

module.exports = {
  rgbToHex: (red, green, blue) => {
    const redHex = red.toString(16) // ff
    const greenHex = green.toString(16)
    const blueHex = blue.toString(16)

    return pad(redHex) + pad(greenHex) + pad(blueHex) // ff0000
  },
  hexToRgb: (hex) => {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
    const redRgb = parseInt(result[1], 16)
    const greenRgb = parseInt(result[2], 16)
    const blueRgb = parseInt(result[3], 16)
    return redRgb +', '+ greenRgb +', '+ blueRgb
  }
}